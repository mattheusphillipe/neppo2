﻿import { Component } from "@angular/core";

@Component
    (

    {
        
        selector: 'user-app', // se der erro era ""
        template: ` <div>
                      <nav class='navbar navbar-inverse'>
                           <div class='container-fluid'>
                             <ul class='nav navbar-nav'>
                               <li><a [routerLink]="['home']">Home</a></li>
                               <li><a [routerLink]="['teste']">Teste</a></li> 
                        
                          </ul>
                          </div>
                     </nav>    
                  <div class='container'>
                    <router-outlet></router-outlet>
                  </div>
                 </div>

                <button class="btn btn-success"> hello Angular </button>
                <button class="btn btn-success"> hello Marianny </button>
                <button class="btn btn-success"> hello hello </button>

    `

        }
    )

export class AppComponent
{

}