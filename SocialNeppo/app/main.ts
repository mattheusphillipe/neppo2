﻿import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app.module'; //referencia de importação para fazer a entrada nos modulos e carregar os aquivos(boorstraping) 


platformBrowserDynamic().bootstrapModule(AppModule);
// usando o plateformbrowserdynamic para inicializar a aplicação do angular carregando os componenentes, serviços e o