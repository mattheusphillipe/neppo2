-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`tab_padrinho`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`tab_padrinho` (
  `id_padrinho` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(60) NULL,
  `data_nasc` DATETIME NULL,
  `cpf` VARCHAR(20) NULL,
  `end` VARCHAR(100) NULL,
  `email` VARCHAR(45) NULL,
  `profissao` VARCHAR(45) NULL,
  PRIMARY KEY (`id_padrinho`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`tab_crianca`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`tab_crianca` (
  `id_crianca` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(60) NULL,
  `data_nasc` DATETIME NULL,
  `presente` VARCHAR(45) NULL,
  `carta` VARCHAR(700) NULL,
  `instituicao` VARCHAR(45) NULL,
  `id_instituicao` INT NOT NULL,
  PRIMARY KEY (`id_crianca`, `id_instituicao`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`tab_doacoes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`tab_doacoes` (
  `id_doacoes` INT NOT NULL,
  `id_crianca` INT NOT NULL,
  `id_padrinho` INT NOT NULL,
  `status` INT NULL,
  PRIMARY KEY (`id_doacoes`, `id_crianca`, `id_padrinho`),
  INDEX `fk_tab_doacoes_tab_crianca1_idx` (`id_crianca` ASC),
  INDEX `fk_tab_doacoes_tab_padrinho1_idx` (`id_padrinho` ASC),
  CONSTRAINT `fk_tab_doacoes_tab_crianca1`
    FOREIGN KEY (`id_crianca`)
    REFERENCES `mydb`.`tab_crianca` (`id_crianca`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tab_doacoes_tab_padrinho1`
    FOREIGN KEY (`id_padrinho`)
    REFERENCES `mydb`.`tab_padrinho` (`id_padrinho`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
